package com.example.vaadindemo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.example.vaadindemo.domain.Competition;

public class CompetitionManager {
	
	private List<Competition> db = new ArrayList<Competition>();
	
	public void addCompetition(Competition competition){
		Competition p = new Competition(competition.getName(), 
                    competition.getType(),
                    competition.getAgeCategory());
		p.setId(UUID.randomUUID());
		db.add(p);
	}
	
	public List<Competition> findAll(){
		return db;
	}

	public void delete(Competition competition) {
		
		Competition toRemove = null;
		for (Competition p: db) {
			if (p.getId().compareTo(competition.getId()) == 0){
				toRemove = p;
				break;
			}
		}
		db.remove(toRemove);		
	}

	public void updateCompetition(Competition competition) {
		// TODO DOIT YOURSELF
		
	}

}
