package com.example.vaadindemo.domain;

import java.util.UUID;

public class Person {
	
	private UUID id;
	
	private String firstName;
	private String lastName;
	private int age;
	private String position;
        private float weight;
        private String competition;
	
	public Person(String firstName, String lastName, int age, String position, float weight, String competition) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
                this.age = age;
                this.position = position;
                this.weight = weight;
                this.competition = competition;
	}

	public Person() {
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
        
        public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
        
        public String getPosition() {
            return this.position;
        }
        
        public void setPosition(String position) {
            this.position = position;
        }
        
        public float getWeight() {
            return this.weight;
        }
        
        public void setWeight(float weight) {
            this.weight = weight;
        }
        
        public String getCompetition() {
            return this.competition;
        }
        
        public void setCompetition(String competition) {
            this.competition = competition;
        }

	@Override
	public String toString() {
		return "Person [firstName=" + firstName + 
                             ", lastName=" + lastName + 
                             ", age=" + age +
                             ", position=" + position +
                             ", weight=" + weight + "]";
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}
