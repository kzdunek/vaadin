package com.example.vaadindemo.domain;

import java.util.UUID;

public class Competition {
    private UUID id;
	
	private String name;
	private String type;
        private String ageCategory;
	
	public Competition(String name, String type, String ageCategory) {
		super();
		this.name = name;
                this.type = type;
                this.ageCategory = ageCategory;
	}

	public Competition() {
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}
        
        public String getAgeCategory() {
		return this.ageCategory;
	}

	public void setAgeCategory(String ageCategory) {
		this.ageCategory = ageCategory;
	}

	@Override
	public String toString() {
		return "Competition [name=" + name + ", type=" + type
				+ ", ageCategory=" + ageCategory + "]";
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}
