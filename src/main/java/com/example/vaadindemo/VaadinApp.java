package com.example.vaadindemo;

import com.example.vaadindemo.domain.Competition;
import com.example.vaadindemo.domain.Person;
import com.example.vaadindemo.service.CompetitionManager;
import com.example.vaadindemo.service.PersonManager;
import com.vaadin.annotations.Title;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.DefaultItemSorter;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;

@Title("Vaadin Demo App")
public class VaadinApp extends UI {

	private static final long serialVersionUID = 1L;

	private final PersonManager personManager = new PersonManager();
        private final CompetitionManager competitionManager = new CompetitionManager();
        
	private final Person person = new Person("Imie", "Nazwisko", 18, "junior(U18)", 0.0f, null);
	private final BeanItem<Person> personItem = new BeanItem<Person>(person);

	private final BeanItemContainer<Person> persons = new BeanItemContainer<Person>(
			Person.class);
        
        
        private final Competition competition = new Competition("nazwa", "typ", "wiek");
	private final BeanItem<Competition> competitionItem = new BeanItem<Competition>(competition);

	private final BeanItemContainer<Competition> competitions = new BeanItemContainer<Competition>(
			Competition.class);
        
        
        private final BeanItemContainer<Person> persons2 = new BeanItemContainer<Person>(
			Person.class);
        private final BeanItemContainer<Person> persons3 = new BeanItemContainer<Person>(
			Person.class);
        
        ComboBox zawody = new ComboBox("zawody");
        ComboBox showcb = new ComboBox("Pokaż listę zadowników dla: ");
        ComboBox zawodyRanking = new ComboBox("Pokaż ranking dla: ");
        
        Button showhideA = new Button("Pokaż/ukryj zawody");
        Button showhideB = new Button("Pokaż/ukryj zawodników");
        Button showhideC = new Button("Pokaż/ukryj ranking");
        
	@Override
	protected void init(VaadinRequest request) {

		Button addCompetitionBtn = new Button("Add");
		Button deleteCompetitionBtn = new Button("Delete");

		VerticalLayout vl = new VerticalLayout();
		setContent(vl);

                final Panel panelA = new Panel("Lista zawodów");
                final Panel panelB = new Panel("Lista zawodników");
                final Panel panelC = new Panel("Ranking");
                FormLayout contentA = new FormLayout();

                
                final ComboBox comboboxA = new ComboBox("Rodzaj zawodów");
                comboboxA.setInvalidAllowed(false);
                comboboxA.setNullSelectionAllowed(false);
                comboboxA.addItem("splawikowe");
                comboboxA.addItem("spinningowe");
                comboboxA.addItem("muchowe");
    
                final ComboBox comboboxB = new ComboBox("Kategoria wiekowa");
                comboboxB.setInvalidAllowed(false);
                comboboxB.setNullSelectionAllowed(false);
                comboboxB.addItem("senior");
                comboboxB.addItem("mlodziez(U23)");
                comboboxB.addItem("junior(U18)");
                comboboxB.addItem("kadet(U14)");

                contentA.addComponent(comboboxA);
                contentA.addComponent(comboboxB);
                
                final TextField t = new TextField("Nazwa zawodów");
                contentA.addComponent(t);
                
                showhideA.addClickListener(new ClickListener() {

                    @Override
                    public void buttonClick(ClickEvent event) {
                        if (panelA.isVisible())
                            panelA.setVisible(false);
                        else
                            panelA.setVisible(true);
                    }
                    
                });
                
                showhideB.addClickListener(new ClickListener() {

                    @Override
                    public void buttonClick(ClickEvent event) {
                        if (panelB.isVisible())
                            panelB.setVisible(false);
                        else
                            panelB.setVisible(true);
                    }
                    
                });
                
                showhideC.addClickListener(new ClickListener() {

                    @Override
                    public void buttonClick(ClickEvent event) {
                        if (panelC.isVisible())
                            panelC.setVisible(false);
                        else
                            panelC.setVisible(true);
                    }
                    
                });
                
                
		addCompetitionBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
                            
                            if(t.getValue().length() < 5 ||
                                    comboboxA.getValue() == null || 
                                    comboboxB.getValue() == null) {
                                Notification.show("Błąd",
                                    "Uzupełnij dane poprawnie",
                                    Notification.Type.WARNING_MESSAGE);
                                return;
                            }
                            
                            Competition tp = new Competition(t.getValue(), (String)comboboxA.getValue(), (String)comboboxB.getValue());
                            
                            competitionManager.addCompetition(tp);
							//personManager.updatePerson(person);
                            competitions.removeAllItems();
                            competitions.addAll(competitionManager.findAll());
                            
                            zawody.removeAllItems();
                            showcb.removeAllItems();
                            zawodyRanking.removeAllItems();
                            for(Competition c : competitionManager.findAll()) {
                                zawody.addItem(c.getName());
                                showcb.addItem(c.getName());
                                zawodyRanking.addItem(c.getName());
                            }
			}
		});

		deleteCompetitionBtn.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				if (!competition.getName().isEmpty()) {
					competitionManager.delete(competition);
					competitions.removeAllItems();
					competitions.addAll(competitionManager.findAll());
				}
			}
		});

		HorizontalLayout hl = new HorizontalLayout();
		hl.addComponent(addCompetitionBtn);
		hl.addComponent(deleteCompetitionBtn);

		final Table competitionsTable = new Table("", competitions);
		competitionsTable.setColumnHeader("name", "Nazwa");
		competitionsTable.setColumnHeader("type", "Rodzaj");
		competitionsTable.setColumnHeader("ageCategory", "Kategoria wiekowa");
		competitionsTable.setSelectable(true);
		competitionsTable.setImmediate(true);

		competitionsTable.addValueChangeListener(new Property.ValueChangeListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent event) {

				Competition selectedCompetition = (Competition) competitionsTable.getValue();
				if (selectedCompetition == null) {
					competition.setName("");
					competition.setType("");
					competition.setAgeCategory("");
					competition.setId(null);
				} else {
					competition.setName(selectedCompetition.getName());
					competition.setType(selectedCompetition.getType());
					competition.setAgeCategory(selectedCompetition.getAgeCategory());
					competition.setId(selectedCompetition.getId());
				}
			}
		});

		contentA.addComponent(hl);
		contentA.addComponent(competitionsTable);
                
                contentA.setSizeUndefined(); // Shrink to fit
                contentA.setMargin(true);
		
                panelA.setContent(contentA);
                vl.addComponent(showhideA);
                vl.addComponent(panelA);
                

                FormLayout contentB = new FormLayout();
                
                final TextField imie = new TextField("Imię");
                final TextField nazwisko = new TextField("nazwisko");
                final TextField wiek = new TextField("wiek");
                final TextField stanowisko = new TextField("stanowisko");
                final TextField waga = new TextField("waga ryb");
                
                
                
                contentB.addComponent(imie);
                contentB.addComponent(nazwisko);
                contentB.addComponent(wiek);
                contentB.addComponent(stanowisko);
                contentB.addComponent(waga);
                contentB.addComponent(zawody);
                
                
                panelB.setContent(contentB);
                vl.addComponent(showhideB);
                vl.addComponent(panelB);
                
                Button addPersonBtn = new Button("Dodaj zawodnika");
		Button deletePersonBtn = new Button("Usuń zawodnika");
                
                addPersonBtn.addClickListener(new ClickListener() {
                    @Override
                    public void buttonClick(ClickEvent event) {
                        
                        if(imie.getValue().length() < 2 ||
                                    nazwisko.getValue().length() < 2 || 
                                    wiek.getValue() == null ||
                                stanowisko.getValue() == null ||
                                waga.getValue() == null ||
                                zawody.getValue().toString().equals("")) {
                                Notification.show("Błąd",
                                    "Uzupełnij dane poprawnie",
                                    Notification.Type.WARNING_MESSAGE);
                                return;
                            }
                        
                        int w = Integer.parseInt(wiek.getValue());
                        float x = Float.parseFloat(waga.getValue());
                        Person tp = new Person(imie.getValue(),
                            nazwisko.getValue(),
                            w,
                            stanowisko.getValue(),
                            x, (String) zawody.getValue());
                            
                        personManager.addPerson(tp);
                        persons.removeAllItems();
                        persons.addAll(personManager.findAll());
                    }
                });
                
                deletePersonBtn.addClickListener(new ClickListener() {

                    @Override
                    public void buttonClick(ClickEvent event) {
                        if (!person.getFirstName().isEmpty()) {
					personManager.delete(person);
					persons.removeAllItems();
					persons.addAll(personManager.findAll());
			}
                    }
                    
                });
                               
                HorizontalLayout hl2 = new HorizontalLayout();
		hl2.addComponent(addPersonBtn);
		hl2.addComponent(deletePersonBtn);
                contentB.addComponent(hl2);
                
                contentB.addComponent(showcb);
                Button showBtn = new Button("Pokaż");
                
                final Panel p = new Panel("Lista");
                
                showBtn.addClickListener(new ClickListener() {

                    @Override
                    public void buttonClick(ClickEvent event) {
                        
                        persons2.removeAllItems();
                        for(Person p : personManager.findAll()) {
                            if(p.getCompetition().equals(showcb.getValue())) {
                                persons2.addBean(p);
                            }
                        }
                        
                        final Table personsTable = new Table("Lista zawodników", persons2);
                        personsTable.setColumnHeader("firstName", "Imię");
                        personsTable.setColumnHeader("lastName", "Nazwisko");
                        personsTable.setColumnHeader("age", "Wiek");
                        personsTable.setColumnHeader("position", "Stanowisko");
                        personsTable.setColumnHeader("weight", "Sumaryczna waga ryb");
                        personsTable.setSelectable(true);
                        personsTable.setImmediate(true);

                        personsTable.addValueChangeListener(new Property.ValueChangeListener() {

                                private static final long serialVersionUID = 1L;

                                @Override
                                public void valueChange(ValueChangeEvent event) {

                                        Person selectedPerson = (Person) personsTable.getValue();
                                        if (selectedPerson == null) {
                                                person.setFirstName("");
                                                person.setLastName("");
                                                person.setAge(0);
                                                person.setPosition("");
                                                person.setWeight(0.0f);
                                                person.setId(null);
                                        } else {
                                                person.setFirstName(selectedPerson.getFirstName());
                                                person.setLastName(selectedPerson.getLastName());
                                                person.setAge(selectedPerson.getAge());
                                                person.setPosition(selectedPerson.getPosition());
                                                person.setWeight(selectedPerson.getWeight());
                                        }
                                }
                        });
                        FormLayout tlay = new FormLayout();
                        tlay.addComponent(personsTable);
                        p.setContent(tlay);
                    }
                });
                
                
                contentB.addComponent(showBtn);
                contentB.addComponent(p);
                
                FormLayout contentC = new FormLayout();
                
                contentC.addComponent(zawodyRanking);
                Button pb = new Button("Pokaż");
                final Panel panel2 = new Panel("Lista");
                
                pb.addClickListener(new ClickListener() {

                    @Override
                    public void buttonClick(ClickEvent event) {
                        persons3.removeAllItems();
                        for(Person p : personManager.findAll()) {
                            if(p.getCompetition().equals(zawodyRanking.getValue())) {
                                persons3.addBean(p);
                            }
                        }
                        persons3.setItemSorter(new DefaultItemSorter() {
                            @Override
                            public int compare(Object o1, Object o2) {
                                return -super.compare(o1, o2);
                            }
                        });
                        persons3.sort(new Object[] { "weight" }, new boolean[] { true });
                        
                        final Table personsTableB = new Table("Lista zawodników", persons3);
                        personsTableB.setColumnHeader("firstName", "Imię");
                        personsTableB.setColumnHeader("lastName", "Nazwisko");
                        personsTableB.setColumnHeader("age", "Wiek");
                        personsTableB.setColumnHeader("position", "Stanowisko");
                        personsTableB.setColumnHeader("weight", "Sumaryczna waga ryb");
                        personsTableB.setSelectable(true);
                        personsTableB.setImmediate(true);

                        personsTableB.addValueChangeListener(new Property.ValueChangeListener() {

                                private static final long serialVersionUID = 1L;

                                @Override
                                public void valueChange(ValueChangeEvent event) {

                                        Person selectedPerson = (Person) personsTableB.getValue();
                                        if (selectedPerson == null) {
                                                person.setFirstName("");
                                                person.setLastName("");
                                                person.setAge(0);
                                                person.setPosition("");
                                                person.setWeight(0.0f);
                                                person.setId(null);
                                        } else {
                                                person.setFirstName(selectedPerson.getFirstName());
                                                person.setLastName(selectedPerson.getLastName());
                                                person.setAge(selectedPerson.getAge());
                                                person.setPosition(selectedPerson.getPosition());
                                                person.setWeight(selectedPerson.getWeight());
                                        }
                                }
                        });

                        FormLayout tlay = new FormLayout();
                        tlay.addComponent(personsTableB);
                        
                        
                       
                        panel2.setContent(tlay);
                    }
                    
                });
                contentC.addComponent(pb);
                contentC.addComponent(panel2);
                panelC.setContent(contentC);
                vl.addComponent(showhideC);
                vl.addComponent(panelC);
                
                panelA.setVisible(false);
                panelB.setVisible(false);
                panelC.setVisible(false);
	}

}